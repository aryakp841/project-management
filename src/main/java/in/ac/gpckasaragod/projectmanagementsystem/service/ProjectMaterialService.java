package in.ac.gpckasaragod.projectmanagementsystem.service;

import in.ac.gpckasaragod.projectmanagementsystem.ui.ProjectMaterialDetailForm;
import java.util.List;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */

/**
 *
 * @author student
 */
public interface ProjectMaterialService {
    public String saveProjectMaterialService(String ProjectId,String MaterialName);

    /**
     *
     * @param id
     * @return
     */
    public ProjectMaterialDetailForm readProjectMaterial(Integer id);
    public List<ProjectMaterialDetailForm>getALLProjectMaterial();
    public String updateProjectMaterial(String ProjectId,String MaterialName);
    public String deleteProjectMaterial(Integer id);
    
}
