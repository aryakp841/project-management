/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.projectmanagementsystem.service.impl;

import in.ac.gpckasaragod.projectmanagementsystem.service.ProjectService;
import in.ac.gpckasaragod.projectmanagementsystem.ui.ProjectDetailForm;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class ProjectServiceImpl extends ConnectionServiceImpl implements ProjectService {

 
    
       

    public String updateProject(Integer id, String projectname, String projectincharge, String projectlocation, String projectdescription, String projects) throws SQLException {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
      try {
                     Connection connection = getConnection();
                Statement statement = connection.createStatement();
                //String id = null;
                String query = "UPDATE PROJECT SET PROJECT_NAME='"+projectname+"',PROJECT_IN_CHARGE'"+projectincharge+"',PROJECT_LOCATION'"+projectlocation+'",PROJECT_DESCRIPTION='"+projectlocation+'",
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
      }catch (SQLException ex) {
            Logger.getLogger(ProjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
      }
     return projects;
    }

    
    
    @Override
   public List<ProjectDetailForm>getAllProject() {
     List<Project> projects = new ArrayList<>();    
     try{
          Connection connection =getConnection();
          Statement statement = connection.createStatement();
          String query = "SELECT * FROM PROJECT";
          ResultSet resultSet = statement.executeQuery(query);
          
          while (resultSet.next()) {
              Integer Id = resultSet.getInt("ID");

          }
      } catch (SQLException ex) {
            Logger.getLogger(ProjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
      }
        List<ProjectDetailForm> projects = null;
         return projects; 
    }

    @Override
    public String saveProject(String projectname, String projectincharge, String projectlocation, String projectdescription) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
     try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
    

            String query = "INSERT INTO PROJECT (ID,PROJECT_NAME,PROJECT_IN_CHARGE,PROJECT_LOCATION,PROJECT_DESCRIPTION) VALUES" + "('" + projectname +"','"+ projectincharge +"','" + projectlocation + "','" + projectdescription + "',)";
            System.out.print(query);
            int status=statement.executeUpdate(query);
            if(status !=1){
                return "Save failed";
            }else {
                return "Save successfully";
            }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
               
           

        }catch (SQLException ex){

            Logger.getLogger(ProjectServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "save failed";
        }
}

    @Override
    public ProjectDetailForm readProject(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
          Project project;
          project = null;
        try {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM PROJECT WHERE ID=" +id;
        ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) {
           id = resultSet.getInt("ID");
          String projectname = resultSet.getString("PROJECT_NAME");
          String projectincharge = resultSet.getString("PROJECT_IN_CHARGE");
          String projectlocation = resultSet.getString("PROJECT_LOCATION");
          String projectdetails = resultSet.getString("PROJECT_DETAILS");
          Project = new Project(id,projectname,projectincharge,projectlocation,projectdetails);
        }
    }   catch (SQLException ex) {
            Logger.getLogger(ProjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
           return Project;
    }
   /* @Override
    public String updateProject(Integer id, String projectname, String projectincharge, String projectlocation, String projectdescription) {
      throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    */

    @Override
    public String deleteProject(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
     try {
         Connection connection = getConnection();
         String query = "DELETE FROM PROJECT WHERE ID =?";
         PreparedStatement statement =connection.prepareStatement(query);
         statement.setInt(1, id);
         int delete = statement.executeUpdate();
         if (delete !=1)
             return "Delete failed";
         else 
             return "Deleed successfully";
     }  catch (SQLException ex) {
            Logger.getLogger(ProjectServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Deleted failed";
        
     }
}

   /* @Override
    public String saveProjectDetailform(String projectname, String projectincharge, String projectlocation, String projectdescription) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Integer getId() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String getProjectName() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String getProjectInCharge() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String getProjectLocation() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public String getProjectDescription() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    }*/

   