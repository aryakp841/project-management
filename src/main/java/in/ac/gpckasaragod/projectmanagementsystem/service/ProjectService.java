/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.projectmanagementsystem.service;

import in.ac.gpckasaragod.projectmanagementsystem.ui.ProjectDetailForm;
import java.util.List;

/**
 *
 * @author student
 */
public interface ProjectService {
    

public String saveProject(String projectname,String projectincharge,String projectlocation,String projectdescription);
   public ProjectDetailForm readProject(Integer id);
   public List<ProjectDetailForm> getAllProject();
   public String updateProject(Integer id,String projectname,String projectincharge,String projectlocation,String projectdescription);
    public String deleteProject(Integer id);

    public String saveProjectDetailform(String projectname, String projectincharge, String projectlocation, String projectdescription);

   /* public Integer getId();

    public String getProjectName();

    public String getProjectInCharge();

    public String getProjectLocation();

    public String getProjectDescription();*/

    public Integer getId();

    public String getProjectName();

    public String getProjectInCharge();

    public String getProjectLocation();

    public String getProjectDescription();

   
}