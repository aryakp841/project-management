/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.projectmanagementsystem.service.impl;

import in.ac.gpckasaragod.projectmanagementsystem.service.ProjectMaterialService;
import in.ac.gpckasaragod.projectmanagementsystem.ui.ProjectMaterialDetailForm;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class ProjectMaterialImpl extends ConnectionServiceImpl implements ProjectMaterialService {

    @Override
    public String saveProjectMaterialService(String ProjectId, String MaterialName) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
           try {
               Connection connection = getConnection();
               Statement statement = connection.createStatement();
               String query = "INSERT INTO PROJECT_MATERIAL (PROJECT_ID,MATERIAL_NAME)VALUES" +"('" + ProjectId +"','" + MaterialName +"')";
               System.err.println("Query:" +query);
               int status = statement.executeUpdate(query);
               if (status !=1) {
                   return "save failed";
               }
               else{
                   return "save successfully";
               }        
                       
           } catch (SQLException ex) {
            Logger.getLogger(ProjectMaterialImpl.class.getName()).log(Level.SEVERE, null, ex);
                  return"save failed";
           }
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public ProjectMaterialDetailForm readProjectMaterial(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
          ProjectMaterialDetailForm projectmaterial;
          projectmaterial = null;
          
        try{
             Connection connection = getConnection();
               Statement statement = connection.createStatement();
               String query ="SELECT * FROM PROJECT_MATERIAL WHERE ID=" +id;
               ResultSet resultSet = statement.executeQuery(query);
               while (resultSet.next()) {
                   id = resultSet.getInt("ID");
                Integer  ProjectId = resultSet.getInt("PROJECT_ID");
                String  MaterialName = resultSet.getString("MATERIAL_NAME"); 
                projectmaterial = new ProjectMaterialDetailForm(id,ProjectId,MaterialName);
               }
               
        } catch (SQLException ex) {
            Logger.getLogger(ProjectMaterialImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return projectmaterial;
    }


    @Override
    public List<ProjectMaterialDetailForm> getALLProjectMaterial() {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
       List<ProjectMaterialDetailForm>projectmaterials = new ArrayList<>();
        try{
             Connection connection = getConnection();
               Statement statement = connection.createStatement();
               String query = "SELECT * FROM PROJECT_MATERIAL";
               ResultSet resultSet = statement.executeQuery(query);
               while(resultSet.next()) {
                   Integer id = resultSet.getInt("ID");
                   Integer ProjectId = resultSet.getInt("PROJECT_ID");
                   String MaterialName = resultSet.getString("MATERIAL_NAME");
                   ProjectMaterialDetailForm projectmaterial = new  ProjectMaterialDetailForm(id,ProjectId,MaterialName);
                   projectmaterials.add(projectmaterial);
               }
    }   catch (SQLException ex) {
            Logger.getLogger(ProjectMaterialImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
            return projectmaterials ;
        }

    public String updateProjectMaterial(Integer ProjectId, String MaterialName, String id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
         try{
             Connection connection = getConnection();
               Statement statement = connection.createStatement();
               String query = "UPDATE PROJECT_MATERIAL SET PROJECT_ID='" + ProjectId +"',MATERIAL_NAME='" + MaterialName +"'WHERE ID=" + id;
               System.out.print(query);
               int update =statement.executeUpdate(query);
               if (update !=1) {
                   return "Update failed";
               }else {
                   return "Update successfully";
               }
               
    }   catch (SQLException ex) {
            Logger.getLogger(ProjectMaterialImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
         return "Update failed";

    }

    /*@Override
    public String updateProjectMaterial(String ProjectId, String MaterialName) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
*/
    @Override
    public String deleteProjectMaterial(Integer id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
         try{
             Connection connection = getConnection();
             String query = "DELETE FROM PROJECT_MATERIAL WHERE ID =?";
             PreparedStatement statement = connection.prepareStatement(query);
             statement.setInt(1, id);
             int delete = statement.executeUpdate();
             if(delete !=1)
                 return "Deleted failed";
             else
                 return "Deleted successfully";
             
         }   catch (SQLException ex) {
            Logger.getLogger(ProjectMaterialImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
         return "Deleted failed";
}

    @Override
    public String updateProjectMaterial(String ProjectId, String MaterialName) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}