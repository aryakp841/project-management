/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.projectmanagementsystem.ui.model.data;

/**
 *
 * @author student
 */
public class ProjectDetails {
     private Integer Id;
  private String ProjectName;
  private String ProjectInCharge;
  private String ProjectLocation;
  private String ProjectDescription;

    public ProjectDetails(Integer Id, String ProjectName, String ProjectInCharge, String ProjectLocation, String ProjectDescription) {
        this.Id = Id;
        this.ProjectName = ProjectName;
        this.ProjectInCharge = ProjectInCharge;
        this.ProjectLocation = ProjectLocation;
        this.ProjectDescription = ProjectDescription;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }

    public String getProjectName() {
        return ProjectName;
    }

    public void setProjectName(String ProjectName) {
        this.ProjectName = ProjectName;
    }

    public String getProjectInCharge() {
        return ProjectInCharge;
    }

    public void setProjectInCharge(String ProjectInCharge) {
        this.ProjectInCharge = ProjectInCharge;
    }

    public String getProjectLocation() {
        return ProjectLocation;
    }

    public void setProjectLocation(String ProjectLocation) {
        this.ProjectLocation = ProjectLocation;
    }

    public String getProjectDescription() {
        return ProjectDescription;
    }

    public void setProjectDescription(String ProjectDescription) {
        this.ProjectDescription = ProjectDescription;
    }

}
